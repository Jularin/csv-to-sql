# CSV-TO-SQL Mircod Internship

### Install poetry
```shell
pip install poetry
```

### Build image, create and start database container
```shell
docker-compose up -d --build
```

### Extract csv files
```shell
cd data && unzip i18n_GeoCSVDump_with_header_qouted_delimiter_comma.zip && cd ..
```

### Install the project dependencies
```shell
cd src && poetry install
```

### Spawn a shell within the virtual environment
```shell
poetry shell
```

### Using a csv_to_sql:
```
python3 manage.py csv_to_sql
```
