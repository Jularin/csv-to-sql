from django.core.management.base import BaseCommand
import csv

from core.models import Country, Region, City


def process_countries():
    with open("../data/_countries.csv") as csv_file:
        countries = csv.DictReader(csv_file)
        for row in countries:
            country = Country(
                id=row["id"],
                title_ru=row['title_ru'],
                title_en=row['title_en'],
                title_es=row['title_es'],
            )
            country.save()


def process_regions():
    with open("../data/_regions.csv") as csv_file:
        regions = csv.DictReader(csv_file)
        for row in regions:
            region = Region(
                id=row['id'],
                country_id=row['country'],
                title_ru=row['title_ru'],
                title_en=row['title_en'],
                title_es=row['title_es'],
            )
            region.save()


def process_cities():
    with open("../data/_cities.csv", encoding="utf-8") as csv_file:
        cities = csv.DictReader(csv_file)
        for row in cities:
            city = City(
                id=row['city_id'],
                country_id=row['country_id'],
                region_id=row['region_id'],
                title_ru=row['title_ru'],
                title_en=row['title_en'],
                title_es=row['title_es'],
            )
            city.save()


class Command(BaseCommand):
    help = "Importing data from csv file type to sql database"

    def handle(self, *args, **kwargs):
        process_countries()
        process_regions()
        process_cities()
