import os

from django.core.asgi import get_asgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "csv_to_sql.settings")

application = get_asgi_application()
